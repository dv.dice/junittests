package tests;


import java.util.ArrayList;
import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;
import itis.quiz.spaceships.SpaceshipFleetManager;
import org.junit.jupiter.api.*;


public class SpaceshipFleetManagerTest
{


    ArrayList<Spaceship> listOfSpaceships = new ArrayList<>();
    SpaceshipFleetManager commandCenter = new CommandCenter();

    public static void main(String[] args)
    {
        SpaceshipFleetManagerTest test = new SpaceshipFleetManagerTest();
        System.out.println("Your score is: " + countScore(test));

    }


    private static Object countScore(SpaceshipFleetManagerTest test)
    {
        int score;
        score = testGetMostPowerfulShip(test);
        score += testGetShipByName(test);
        score += testGetAllShipsWithEnoughCargoSpace(test);
        score += testGetAllCivilianShips(test);
            if(score <= 5){
                return (score);
            }if((score <= 7) && (score >5)){
                return (score);
            }if (score > 7){
                return (score);
            }


        return null;
    }

    private static int testGetMostPowerfulShip(SpaceshipFleetManagerTest test){
        boolean[] res = new boolean[3];
        int score = 0;
        res[0] = test.getMostPowerfulShip_isReturnsMostPowerful();
        System.out.println("Test result = " + res[0]);
        res[1] = test.getMostPowerfulShip_isReturnsFirstOfPowerfulShips();
        System.out.println("Test result = " + res[1]);
        res[2] = test.getMostPowerfulShip_isReturnsNullWhenThereAreNoMatchingShips();
        System.out.println("Test result = " + res[2]);
        System.out.println();
        for (boolean isTrue : res){
            if (isTrue){
                score++;
            }
        }
        return score;
    }


    private boolean getMostPowerfulShip_isReturnsMostPowerful(){
        listOfSpaceships.add(new Spaceship("Foo", 987, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 10200543,0,0));
        listOfSpaceships.add(new Spaceship("Boom",4456,0,0));
        Spaceship result = commandCenter.getMostPowerfulShip(listOfSpaceships);
        if ((result.getName().equals("Bar")) && (result.getFirePower()==10200543)){
            listOfSpaceships.clear();
            return true;
        }else{
            listOfSpaceships.clear();
            return false;
        }
    }

    private boolean getMostPowerfulShip_isReturnsFirstOfPowerfulShips(){
        listOfSpaceships.add(new Spaceship("Foo", 1000, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 1000,0,0));
        Spaceship result = commandCenter.getMostPowerfulShip(listOfSpaceships);
        if ((result.getName().equals("Foo")) && (result.getFirePower()==1000)){
            listOfSpaceships.clear();
            return true;
        }else{
            listOfSpaceships.clear();
            return false;
        }
    }

    private boolean getMostPowerfulShip_isReturnsNullWhenThereAreNoMatchingShips(){
        listOfSpaceships.add(new Spaceship("Foo", 0, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 0,0,0));
        listOfSpaceships.add(new Spaceship("Lorem",0,0,0));
        Spaceship result = commandCenter.getMostPowerfulShip(listOfSpaceships);
        if (result == null){
            listOfSpaceships.clear();
            return true;
        }
        else{
            listOfSpaceships.clear();
            return false;
            }
    }

    private static int testGetShipByName(SpaceshipFleetManagerTest test){
        boolean[] res = new boolean[2];
        int score = 0;
        res[0] = test.getShipByName_isReturnsShipByName();
        System.out.println("Test result = " + res[0]);
        res[1] = test.getShipByName_isReturnsNullWhenThereAreNoMatchingShips();
        System.out.println("Test result = " + res[1]);
        System.out.println();
        for (boolean isTrue : res){
            if (isTrue){
                score++;
            }
        }
        return score;
    }

    private boolean getShipByName_isReturnsShipByName(){
        listOfSpaceships.add(new Spaceship("Foo", 0, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 0,0,0));
        Spaceship result = commandCenter.getShipByName(listOfSpaceships, "Bar");
        if ((result != null) && (result.getName().equals("Bar"))){
            listOfSpaceships.clear();
            return true;
        }else{
            listOfSpaceships.clear();
            return false;
        }
    }

    private boolean getShipByName_isReturnsNullWhenThereAreNoMatchingShips(){
        listOfSpaceships.add(new Spaceship("Foo", 0, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 0,0,0));
        Spaceship result = commandCenter.getShipByName(listOfSpaceships, "abc");
        if (result == null){
            listOfSpaceships.clear();
            return true;
        }else{
            listOfSpaceships.clear();
            return false;
        }
    }


    private static int testGetAllShipsWithEnoughCargoSpace(SpaceshipFleetManagerTest test){
        boolean[] res = new boolean[2];
        int score = 0;
        res[0] = test.getAllShipsWithEnoughCargoSpace_isReturnsAllShipsWithEnoughCargoSpace();
        System.out.println("Test result = " + res[0]);
        res[1] = test.getAllShipsWithEnoughCargoSpace_isReturnsNullWhenThereAreNoShipsWithEnoughCargoSpace();
        System.out.println("Test result = " + res[1]);
        System.out.println();
        for (boolean isTrue : res){
            if (isTrue){
                score++;
            }
        }
        return score;
    }

    private boolean getAllShipsWithEnoughCargoSpace_isReturnsAllShipsWithEnoughCargoSpace(){
        Spaceship bar = new Spaceship("Bar", 0,500,0);
        Spaceship boom = new Spaceship("Boom", 0,600,0);
        listOfSpaceships.add(new Spaceship("Foo", 0, 20,0));
        listOfSpaceships.add(bar);
        listOfSpaceships.add(boom);
        ArrayList<Spaceship> shipsWithEnoughCargoSpace = new ArrayList<>();
        shipsWithEnoughCargoSpace.add(bar);
        shipsWithEnoughCargoSpace.add(boom);
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(listOfSpaceships,500);
        if (result.equals(shipsWithEnoughCargoSpace)) {
            listOfSpaceships.clear();
            return true;
        }else{
            listOfSpaceships.clear();
            return false;
        }
    }

    private boolean getAllShipsWithEnoughCargoSpace_isReturnsNullWhenThereAreNoShipsWithEnoughCargoSpace() {
        listOfSpaceships.add(new Spaceship("Foo", 0, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 0,0,0));
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(listOfSpaceships,500);
        if (result == null){
            listOfSpaceships.clear();
            return true;
        }else{
            listOfSpaceships.clear();
            return false;
        }
    }


    private static int testGetAllCivilianShips(SpaceshipFleetManagerTest test){
        int score = 0;
        boolean[] res = new boolean[2];
        res[0] = test.getAllCiviliansShips_isReturnsAllCiviliansShips();
        System.out.println("Test result = " + res[0]);
        res[1] = test.getAllCiviliansShips_isReturnNullWhenThereAreNoMatchingShips();
        System.out.println("Test result = " + res[1]);
        System.out.println();
        for (boolean isTrue : res){
            if (isTrue){
                score++;
            }
        }
        return score;
    }

    private boolean getAllCiviliansShips_isReturnsAllCiviliansShips(){

        Spaceship foo = new Spaceship("Foo", 0, 0,0);
        Spaceship bar = new Spaceship("Bar", 0, 0,0);
        ArrayList<Spaceship> test = new ArrayList<>();
        test.add(foo);
        test.add(bar);
        listOfSpaceships.add(foo);
        listOfSpaceships.add(bar);
        listOfSpaceships.add(new Spaceship("Boom",1337,0,0));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(listOfSpaceships);
        if (test.equals(result)){
            listOfSpaceships.clear();
            return true;
        }else{
            listOfSpaceships.clear();
            return false;
        }
    }

    private boolean getAllCiviliansShips_isReturnNullWhenThereAreNoMatchingShips(){
        listOfSpaceships.add(new Spaceship("Foo",2607,0,0));
        listOfSpaceships.add(new Spaceship("Bar",2002,0,0));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(listOfSpaceships);
        if (result == null){
            listOfSpaceships.clear();
            return true;
        }else{
            listOfSpaceships.clear();
            return false;
        }
    }

}
