package tests;

import java.util.ArrayList;
import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;
import itis.quiz.spaceships.SpaceshipFleetManager;
import org.junit.jupiter.api.*;


public class TestWithJUnit{


    static ArrayList<Spaceship> listOfSpaceships = new ArrayList<>();
    SpaceshipFleetManager commandCenter = new CommandCenter();
//    @AfterEach
//    void cleaner(){
//        listOfSpaceships.clear();
//    }


    @DisplayName("Существует ли самый мощный корабль?")
    @Test
    void getMostPowerfulShip_isExists(){
        listOfSpaceships.add(new Spaceship("Foo", 987, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 10200543,0,0));
        listOfSpaceships.add(new Spaceship("Lorem",4456,0,0));
        Spaceship result = commandCenter.getMostPowerfulShip(listOfSpaceships);
        Assertions.assertEquals("Bar", result.getName());
    }


    @DisplayName("Возвращает ли первый из самых мощных?")
    @Test
    void getMostPowerfulShip_isReturnsFirstOfTheMostPowerfulShips(){
        listOfSpaceships.add(new Spaceship("Foo", 999, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 999,0,0));
        Spaceship result = commandCenter.getMostPowerfulShip(listOfSpaceships);
        Assertions.assertEquals("Foo", result.getName());
    }


    @DisplayName("null если мощность==0")
    @Test
    void getMostPowerfulShip_isReturnsNullWhenThereAreNoMatchingShips() {
        listOfSpaceships.add(new Spaceship("Foo", 0, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 0,0,0));
        listOfSpaceships.add(new Spaceship("Lorem",0,0,0));
        Spaceship result = commandCenter.getMostPowerfulShip(listOfSpaceships);
        Assertions.assertNull(result);
    }


    @DisplayName("Возвращает ли по названию?")
    @Test
    void getShipByName_isReturnsShipByName(){
        listOfSpaceships.add(new Spaceship("Foo", 0, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 0,0,0));
        Spaceship result = commandCenter.getShipByName(listOfSpaceships, "Bar");
        Assertions.assertEquals("Bar", result);
    }



    @DisplayName("Вернуть null, если нет названия")
    @Test
    void getShipByName_isReturnsNullWhenThereAreNoMatchingShips(){
        listOfSpaceships.add(new Spaceship("Foo", 0, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 0,0,0));
        Spaceship result = commandCenter.getShipByName(listOfSpaceships, "Lorem");
        Assertions.assertNull(result);
    }



    @DisplayName("Возвращает ли все корабли с достаточным грузовым отсеком")
    @Test
    private boolean getAllShipsWithEnoughCargoSpace_isReturnsAllShipsWithEnoughCargoSpace(){
        Spaceship bar = new Spaceship("Bar", 0,500,0);
        Spaceship lorem = new Spaceship("Lorem", 0,600,0);
        listOfSpaceships.add(new Spaceship("Foo", 0, 20,0));
        listOfSpaceships.add(bar);
        listOfSpaceships.add(lorem);
        ArrayList<Spaceship> shipsWithEnoughCargoSpace = new ArrayList<>();
        shipsWithEnoughCargoSpace.add(bar);
        shipsWithEnoughCargoSpace.add(lorem);
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(listOfSpaceships,500);
        if (result.equals(shipsWithEnoughCargoSpace)){
            listOfSpaceships.clear();
            return true;
        }else{
            listOfSpaceships.clear();
            return false;
        }
    }



    @DisplayName("Возвращает ли null, если нет кораблей с достаточным грузовым отсеком?")
    @Test
    void getAllShipsWithEnoughCargoSpace_isReturnsNullWhenThereAreNoShipsWithEnoughCargoSpace(){
        listOfSpaceships.add(new Spaceship("Foo", 0, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 0,0,0));
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(listOfSpaceships,500);
        Assertions.assertEquals(result.size(), 0);
    }





    @DisplayName("Возвращает ли все мирные корабли?")
    @Test
    void getAllCiviliansShips_isReturnsAllCiviliansShips(){
        listOfSpaceships.add( new Spaceship("Foo", 0, 0,0));
        listOfSpaceships.add(new Spaceship("Bar", 0, 0,0));
        listOfSpaceships.add(new Spaceship("Boom",1337,0,0));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(listOfSpaceships);
        boolean isTrue = true;
        for (Spaceship spaceship : result){
            if (spaceship.getFirePower() > 0){
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue);

    }

    @DisplayName("Возвращает ли null, если нет мирных?")
    @Test
    void getAllCiviliansShips_isReturnNullWhenThereAreNoMatchingShips(){
        listOfSpaceships.add(new Spaceship("Foo",2607,0,0));
        listOfSpaceships.add(new Spaceship("Bar",2002,0,0));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(listOfSpaceships);
        Assertions.assertEquals(result.size(), 0);
    }

}

