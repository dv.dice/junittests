package itis.quiz.spaceships;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {
    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        Spaceship spaceship = new Spaceship("null", 0, 0, 0);
        int max = 0;
        for (Spaceship ship : ships) {
            if (ship.getFirePower() > max){
                max = ship.getFirePower();
                spaceship = ship;
            }
        }
        if (spaceship.getFirePower() == 0) {
            return null;
        } else {
            return spaceship;

        }
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for (Spaceship ship : ships) {
            if (ship.getName().equals(name)) {
                return ship;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> shipsWithEnoughCargoSpace = new ArrayList<>();
        boolean isExist = false;
        for (Spaceship ship : ships) {
            if (ship.getCargoSpace() >= cargoSize) {
                shipsWithEnoughCargoSpace.add(ship);
                isExist = true;
            }
        }
        if (!isExist) {
            return null;
        }
        return shipsWithEnoughCargoSpace;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> civilianShips = new ArrayList<>();
        boolean isExist = false;
        for (Spaceship ship : ships)
        {
            if (ship.getFirePower() == 0)
            {
                civilianShips.add(ship);
                isExist = true;
            }
        }
        if (isExist)
        {
            return civilianShips;
        }
        else
            {
            return null;
            }
    }
}
